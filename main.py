import pandas
from apyori import apriori
from sklearn import model_selection

if __name__ == '__main__':
    data = pandas.read_csv('store_data.csv')
    print (data)
    item_list = data.unstack().dropna().unique()
    print ("# items:", len(item_list))

    train, test = model_selection.train_test_split(data, test_size= 0.1)

    train = train.T.apply(lambda x: x.dropna().tolist()).tolist()

    result = list(apriori(train))
    print (result)

    for rule in result:
        print (rule[0])
        print ("--------------------------------------")

